import {
  SET_TWEETS,
  LOADING_DATA,
  LIKE_TWEET,
  UNLIKE_TWEET,
  DELETE_TWEET,
  SET_ERRORS,
  POST_TWEET,
  CLEAR_ERRORS,
  LOADING_UI,
  SET_TWEET,
  STOP_LOADING_UI,
  SUBMIT_COMMENT
} from "../types";
import axios from "axios";

// Get all tweet
export const getTweets = () => dispatch => {
  dispatch({ type: LOADING_DATA });
  axios
    .get("/screams")
    .then(res => {
      dispatch({
        type: SET_TWEETS,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: SET_TWEETS,
        payload: []
      });
    });
};
export const getTweet = tweetId => dispatch => {
  dispatch({ type: LOADING_UI });
  axios
    .get(`/scream/${tweetId}`)
    .then(res => {
      dispatch({
        type: SET_TWEET,
        payload: res.data
      });
      dispatch({ type: STOP_LOADING_UI });
    })
    .catch(err => console.log(err));
};
// Post a tweet
export const postTweet = newTweet => dispatch => {
  dispatch({ type: LOADING_UI });
  axios
    .post("/scream", newTweet)
    .then(res => {
      dispatch({
        type: POST_TWEET,
        payload: res.data
      });
      dispatch(clearErrors());
    })
    .catch(err => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};
// Like a tweet
export const likeTweet = tweetId => dispatch => {
  axios
    .get(`/scream/${tweetId}/like`)
    .then(res => {
      dispatch({
        type: LIKE_TWEET,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};
// Unlike a tweet
export const unlikeTweet = tweetId => dispatch => {
  axios
    .get(`/scream/${tweetId}/unlike`)
    .then(res => {
      dispatch({
        type: UNLIKE_TWEET,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};
// Submit a comment
export const submitComment = (tweetId, commentData) => dispatch => {
  axios
    .post(`/scream/${tweetId}/comment`, commentData)
    .then(res => {
      dispatch({
        type: SUBMIT_COMMENT,
        payload: res.data
      });
      dispatch(clearErrors());
    })
    .catch(err => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data
      });
    });
};
export const deleteTweet = tweetId => dispatch => {
  axios
    .delete(`/scream/${tweetId}`)
    .then(() => {
      dispatch({ type: DELETE_TWEET, payload: tweetId });
    })
    .catch(err => console.log(err));
};

export const getUserData = userHandle => dispatch => {
  dispatch({ type: LOADING_DATA });
  axios
    .get(`/user/${userHandle}`)
    .then(res => {
      dispatch({
        type: SET_TWEETS,
        payload: res.data.tweets
      });
    })
    .catch(() => {
      dispatch({
        type: SET_TWEETS,
        payload: null
      });
    });
};

export const clearErrors = () => dispatch => {
  dispatch({ type: CLEAR_ERRORS });
};
